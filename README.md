# README #

The purpose of the project is to aid in the ability to include the version number as part of the output.msi filename in a Visual Studio Setup Project?

Once you have compiled and have the msirenamer.exe in the setup project in the post build you need to add the following:

```
#!script

[MsiRenamerAppPath]\MsiRenamer.exe "$(BuildOutputPath)"

```

### Other Notes ###

This source code was originally obtained from the following:

[http://stackoverflow.com/questions/3359974/how-to-include-version-number-in-vs-setup-project-output-filename
](Link URL)

and to get it to run on VS 2015 i also need to add a reference
    to **%WINDIR%\system32\msi.dll** as i had issue trying to add a COM reference to **Microsoft Windows Installer Object Library**.