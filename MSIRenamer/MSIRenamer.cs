﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using WindowsInstaller;

class MsiRenamer
{
    static void Main(string[] args)
    {
        string inputFile;
        string productName = "[ProductName]";

        if (args.Length == 0)
        {
            Console.WriteLine("Enter MSI file:");
            inputFile = Console.ReadLine();
        }
        else
        {
            inputFile = args[0];
        } 

        System.IO.FileInfo fi = new System.IO.FileInfo(inputFile);
        string path = fi.DirectoryName;

        try
        {
            string version;

            if (inputFile.EndsWith(".msi", StringComparison.OrdinalIgnoreCase))
            {
                // Read the MSI property
                version = GetMsiProperty(inputFile, "ProductVersion");
                productName = GetMsiProperty(inputFile, "ProductName");
            }
            else
            {
                return;
            }
            // Edit: MarkLakata: .msi extension is added back to filename
            File.Copy(inputFile, string.Format("{0}\\{1}_{2}.msi", path , productName, version));
            File.Delete(inputFile);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    static string GetMsiProperty(string msiFile, string property)
    {
        string retVal = string.Empty;

        // Create an Installer instance  
        Type classType = Type.GetTypeFromProgID("WindowsInstaller.Installer");
        Object installerObj = Activator.CreateInstance(classType);
        Installer installer = installerObj as Installer;

        // Open the msi file for reading  
        // 0 - Read, 1 - Read/Write  
        Database database = installer.OpenDatabase(msiFile, 0);

        // Fetch the requested property  
        string sql = String.Format(
            "SELECT Value FROM Property WHERE Property='{0}'", property);
        View view = database.OpenView(sql);
        view.Execute(null);

        // Read in the fetched record  
        Record record = view.Fetch();
        if (record != null)
        {
            retVal = record.get_StringData(1);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(record);
        }
        view.Close();
        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(view);
        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(database);

        return retVal;
    }
}